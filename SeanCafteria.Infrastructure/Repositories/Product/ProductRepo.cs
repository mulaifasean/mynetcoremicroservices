﻿using Dapper;
using SeanCafteria.Products.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace SeanCafteria.Products.Infrastructure.Repositories.Product
{
    public class ProductRepo : IProductRepo
    {
        private readonly string _connectionstr = string.Empty;
        private readonly string connectionstr = "Server=ICT-2XHL532-PC;Database=university;Trusted_Connection=True;";
        public ProductRepo()
        {
            _connectionstr = connectionstr;
        }   

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionstr);
            }
        }

        public Domain.Models.Product GetProduct(int Product_ID)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    return dbConnection.Query<Domain.Models.Product>("Get_Product", new { ProductID = Product_ID }, null, true, null, CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch
            {
                throw new NotImplementedException(); 
            }          
        }

        public List<Domain.Models.Product> GetProducts()
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    return dbConnection.Query<Domain.Models.Product>("Get_All_Products", commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
            
        }

        public void InsertProduct(Domain.Models.Product product)
        {
            try
            {
                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();
                    dbConnection.Execute("product_insert", new { product.Name, product.Quantity, product.Price }, commandType: CommandType.StoredProcedure);
                }
            }
            catch
            {
                throw new NotImplementedException();
            }
        }
    }
}
