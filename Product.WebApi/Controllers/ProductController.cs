﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SeanCafteria.Products.Interface.Contracts;
using SeanCafteria.Products.Interface.Request;
using SeanCafteria.Products.Interface.Response;

namespace SeanCafteria.Products.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;

        private readonly IProductService _productService;
        public ProductController(IProductService productService, ILogger<ProductController> logger)
        {
            _productService = productService;
            _logger = logger;
        }
        // GET: api/Product
        [HttpGet]
        [Route("getProducts")]
        public List<ProductResponse> GetProducts()
        {
            return _productService.GetProducts();
        }

        // GET: api/Product/5
        [HttpGet]
        [Route("GetProduct" + "/" + "{Product_ID}")]
        public ProductResponse GetProduct(int Product_ID)
        {
            return _productService.GetProduct(Product_ID);
        }

        // POST: api/Product
        [HttpPost]
        [Route("InsertProduct")]
        public void InsertProduct([FromBody] ProductRequest productRequest)
        {
            _productService.InsertProduct(productRequest);
        }

    }
}
