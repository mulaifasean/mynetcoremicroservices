﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeanCafteria.Products.Domain.Models
{
     public class Product
    {
        public int Product_ID { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }

        public float Price { get; set; }

    }
}
