﻿using System.Collections.Generic;
using SeanCafteria.Products.Domain.Models;

namespace SeanCafteria.Products.Domain.Contracts
{
    public interface IProductRepo
    {
        void InsertProduct(Product product);

        List<Product> GetProducts();

        Product GetProduct(int Product_ID);
    }
}
