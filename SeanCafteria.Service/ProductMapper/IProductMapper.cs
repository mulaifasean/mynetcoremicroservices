﻿using SeanCafteria.Products.Domain.Models;
using SeanCafteria.Products.Interface.Request;
using SeanCafteria.Products.Interface.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeanCafteria.Products.Service.ProductMapper
{
    public interface IProductMapper
    {
        Product MapToProductDomain(ProductRequest productRequest);

        ProductResponse MapToProductResponse(Product product);

        List<ProductResponse> MapToProductResponse(List<Product> products);

    }
}
