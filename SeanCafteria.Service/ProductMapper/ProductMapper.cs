﻿using System;
using System.Collections.Generic;
using System.Text;
using SeanCafteria.Products.Domain.Models;
using SeanCafteria.Products.Interface.Request;
using SeanCafteria.Products.Interface.Response;

namespace SeanCafteria.Products.Service.ProductMapper
{
    public class ProductMapper : IProductMapper
    {
        public Product MapToProductDomain(ProductRequest productRequest)
        {
            return new Product
            {
                Name     = productRequest.Name,
                Quantity = productRequest.Quantity,
                Price    = productRequest.Price
            };
        }

        public ProductResponse MapToProductResponse(Product product)
        {
            return new ProductResponse
            {
                Name     = product.Name,
                Quantity = product.Quantity,
                Price    = product.Price
            };
        }

        public List<ProductResponse> MapToProductResponse(List<Product> products)
        {
            var response = new List<ProductResponse>();
            foreach (var product in products)
            {
                response.Add(new ProductResponse
                {
                    Name = product.Name,
                    Quantity = product.Quantity,
                    Price = product.Price
                });
            }
            return response;
        }
    }
}
