﻿
using SeanCafteria.Products.Domain.Contracts;
using SeanCafteria.Products.Interface.Contracts;
using SeanCafteria.Products.Interface.Request;
using SeanCafteria.Products.Interface.Response;
using SeanCafteria.Products.Service.ProductMapper;
using System;
using System.Collections.Generic;

namespace SeanCafteria.Products.Service.InternalService
{
    public class ProductService : IProductService
    {
        private readonly IProductMapper _productMapper;
        private readonly IProductRepo   _productRepo;

        public ProductService(IProductMapper productMapper,IProductRepo productRepo)
        {
            _productMapper = productMapper;
            _productRepo = productRepo;
        }              
        ProductResponse IProductService.GetProduct(int Product_ID)
        {
             var productvar = _productRepo.GetProduct(Product_ID);
             var prodresult = _productMapper.MapToProductResponse(productvar);
             return prodresult;           
        }

         void IProductService.InsertProduct(ProductRequest productRequest) 
        {
             var productvar = _productMapper.MapToProductDomain(productRequest);
             _productRepo.InsertProduct(productvar);                  
        }

        List<ProductResponse> IProductService.GetProducts()
        {
             var productList = _productRepo.GetProducts();
             var result = _productMapper.MapToProductResponse(productList);
             return result;          
        }
    }
}
