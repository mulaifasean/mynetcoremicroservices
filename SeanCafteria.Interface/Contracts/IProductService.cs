﻿using SeanCafteria.Products.Interface.Request;
using SeanCafteria.Products.Interface.Response;
using System.Collections.Generic;

namespace SeanCafteria.Products.Interface.Contracts
{
    public interface IProductService
    {
        ProductResponse GetProduct(int Product_ID);

        void InsertProduct(ProductRequest productRequest);

        List<ProductResponse> GetProducts();
    }
}
