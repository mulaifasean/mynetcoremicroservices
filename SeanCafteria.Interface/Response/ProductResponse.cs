﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SeanCafteria.Products.Interface.Response
{
     public class ProductResponse
    {
        public string Name { get; set; }

        public int Quantity { get; set; }

        public float Price { get; set; }

    }
}
